const chai = require('chai')
const chaiHttp = require('chai-http')
const jwt = require('jsonwebtoken')
const secretKey = require('../src/config/appconfig').secretKey

// LET OP: voeg onder aan app.js toe: module.exports = app
const server = require('../app')

chai.should()
chai.use(chaiHttp)

const endpointToTest = '/api/apartments'
const authorizationHeader = 'Authorization'
let token

//
// Deze functie wordt voorafgaand aan alle tests 1 x uitgevoerd.
//
before(() => {
  console.log('- before')
  // We hebben een token nodig om een ingelogde gebruiker te simuleren.
  // Hier kiezen we ervoor om een token voor UserId 1 te gebruiken.
  const payload = {
    UserId: 1
  }
  jwt.sign({ data: payload }, secretKey, { expiresIn: 2 * 60 }, (err, result) => {
    if (result) {
      token = result
    }
  })
})

beforeEach(() => {
  console.log('- beforeEach')
})

describe('POST: api/apartments/:id', () => {
  it('Zou een valide id moeten teruggeven mits het meegegeven object valide is.', done => {
    chai
      .request(server)
      .post(endpointToTest)
      .set(authorizationHeader, 'Bearer ' + token)
      .send({
        Description: 'Hogeschool Avans',
        StreetAddress: 'Buitendijk 19',
        PostalCode: '3292 LA',
        City: 'Strijensas'
      })
      .end((err, res) => {
        res.should.have.status(200)
        res.body.should.be.a('object')

        const result = res.body.result
        result.should.be.an('array').that.has.length(1)
        const apartment = result[0]
        apartment.should.have.property('Description')
        apartment.should.have.property('StreetAddress')
        apartment.should.have.property('Description')
        apartment.should.not.have.property('password')
        done()
      })
  })

  it('should throw an error when the database is full', done => {
    done()
  })
})

describe('Movie API GET', () => {
  it('should return an array of Movies', done => {
    // Write your test here
    done()
  })
})

describe('Movie API PUT', () => {
  it('should return the updated Movie when providing valid input', done => {
    // Write your test here
    done()
  })
})

describe('Movie API DELETE', () => {
  it('should return http 200 when deleting a Movie with existing id', done => {
    // Write your test here
    done()
  })
})
