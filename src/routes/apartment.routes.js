const express = require('express')
const router = express.Router()
const ApartmentController = require('../controllers/apartment.controller')
const AuthController = require('../controllers/authentication.controller')

// Lijst van apartments
router.post('/', AuthController.validateToken, ApartmentController.createApartment)
router.get('/', ApartmentController.getAllApartments)
router.get('/:apartmentId', ApartmentController.getApartmentById)
router.put('/:apartmentId', AuthController.validateToken, ApartmentController.setApartmentById)
router.delete('/:apartmentId', AuthController.validateToken, ApartmentController.deleteApartmentById)

module.exports = router
