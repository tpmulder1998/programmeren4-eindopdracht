const ReservationController = require('../controllers/reservation.controller')
const express = require('express')
const router = express.Router()
const AuthController = require('../controllers/authentication.controller')

// Lijst van reservations
router.post('/:apartmentId/reservations', AuthController.validateToken, ReservationController.createReservation)
router.get('/:apartmentId/reservations', ReservationController.getAllReservations)
router.get('/:apartmentId/reservations/:reservationId', ReservationController.getReservationById)
router.put('/:apartmentId/reservations/:reservationId', AuthController.validateToken, ReservationController.setReservationById)
router.delete('/:apartmentId/reservations/:reservationId', AuthController.validateToken, ReservationController.deleteReservationById)

module.exports = router