const logger = require('../config/appconfig').logger
const database = require('../datalayer/mssql.dao')
const assert = require('assert')

module.exports = {
  createApartment: function(req, res, next) {
    logger.info('Post /api/apartments aangeroepen')

    // Check dat we de UserId in het request hebben - vanuit de validateToken functie.
    logger.trace('User id:', req.userId)

    // hier komt in het request een apartment binnen.
    const apartment = req.body
    logger.info(apartment)
    try {
      // Valideer hier de properties van apartment.
      assert.equal(typeof apartment.Description, 'string', 'apartment.Description is verplicht')
      assert.equal(typeof apartment.StreetAddress, 'string', 'apartment.StreetAddress is verplicht')
      assert.equal(typeof apartment.PostalCode, 'string', 'apartment.PostalCode is verplicht')
      assert.equal(typeof apartment.City, 'string', 'apartment.City is verplicht')

    } catch (e) {
      const errorObject = {
        message: e.toString(),
        code: 400
      }
      return next(errorObject)
    }

    const query =
      "INSERT INTO Apartment(Description, StreetAddress, PostalCode, City, UserId) VALUES ('" +
      apartment.Description +
      "','" +
      apartment.StreetAddress +
      "','" +
      apartment.PostalCode +
      "','" +
      apartment.City +
      "','" +
      req.userId +
      "'); SELECT (" +
      'SELECT a.ApartmentId, Description, ' +
        '(SELECT * FROM DBUser u WHERE u.UserId = a.UserId FOR JSON PATH) AS Landlord, ' +
        '(SELECT * FROM Reservation r WHERE r.ApartmentId = a.ApartmentId FOR JSON PATH) AS Reservations ' +
      'FROM Apartment a WHERE a.ApartmentId = SCOPE_IDENTITY() FOR JSON PATH) AS result'

    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        const obj = rows.recordset[0].result
        const response = JSON.parse(obj)
        res.status(200).json({ result: response })
      }
    })
  },

  getAllApartments: (req, res, next) => {
    logger.info('Get /api/apartments aangeroepen')

    const query =
    'SELECT (' +
      'SELECT a.ApartmentId, Description, ' +
        '(SELECT * FROM DBUser u WHERE u.UserId = a.UserId FOR JSON PATH) AS Landlord, ' +
        '(SELECT * FROM Reservation r WHERE r.ApartmentId = a.ApartmentId FOR JSON PATH) AS Reservations ' +
      'FROM Apartment a FOR JSON PATH) AS result'
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        var obj = rows.recordset[0].result
        var response = JSON.parse(obj)
        res.status(200).json({result: response})
      }
    })
  },

  getApartmentById: function(req, res, next) {
    logger.info('Get /api/apartments/:id aangeroepen')
    const aId = req.params.apartmentId

    const query = 'SELECT (' +
    'SELECT a.ApartmentId, Description, ' +
      '(SELECT * FROM DBUser u WHERE u.UserId = a.UserId FOR JSON PATH) AS Landlord, ' +
      '(SELECT * FROM Reservation r WHERE r.ApartmentId = a.ApartmentId FOR JSON PATH) AS Reservations ' +
    `FROM Apartment a WHERE a.ApartmentId = ${aId} FOR JSON PATH) AS result`
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        var obj = rows.recordset[0].result
        var response = JSON.parse(obj)
        res.status(200).json({result: response})
      }
    })
  },

  setApartmentById: function(req, res, next) {
    logger.info('PUT: api/apartments/:id')

    const id = req.params.apartmentId
    const userId = req.userId

    const apartment = req.body

    try {
      // Valideer hier de properties van apartment.
      assert.equal(typeof apartment.Description, 'string', 'apartment.Description is verplicht')
      assert.equal(typeof apartment.StreetAddress, 'string', 'apartment.StreetAddress is verplicht')
      assert.equal(typeof apartment.PostalCode, 'string', 'apartment.PostalCode is verplicht')
      assert.equal(typeof apartment.City, 'string', 'apartment.City is verplicht')

    } catch (e) {
      const errorObject = {
        message: e.toString(),
        code: 400
      }
      return next(errorObject)
    }

    const query = 'UPDATE Apartment SET ' +
    `Description = '${apartment.Description}', StreetAddress = '${apartment.StreetAddress}', PostalCode = '${apartment.PostalCode}', City = '${apartment.City}', UserId = ${userId} WHERE Apartment.ApartmentId = ${id}` 
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        logger.trace('Could not update apartment: ', err)
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        if (rows.rowsAffected[0] === 0) {
          // query ging goed, maar geen appartement met de gegeven ApartmentId EN userId.
          // -> retourneer een error!
          const msg = 'Appartement niet gevonden of je hebt geen permissie om deze aan te passen!'
          logger.trace(msg)
          const errorObject = {
            message: msg,
            code: 401
          }
          next(errorObject)
        } else {
          const msg = 'Appartement geupdate!'
          res.status(200).json({ message: msg })
        }
      }
    })
  },

  deleteApartmentById: function(req, res, next) {
    logger.info('deleteById aangeroepen')
    const id = req.params.apartmentId
    const userId = req.userId

    const query = `DELETE FROM Apartment WHERE ApartmentId=${id} AND UserId=${userId}`
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        logger.trace('Kon het appartement niet verwijderen', err)
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        if (rows.rowsAffected[0] === 0) {
          // query ging goed, maar geen appartement met de gegeven ApartmentId EN userId.
          // -> retourneer een error!
          const msg = 'Appartement niet gevonden of je hebt niet het recht deze te verwijderen!'
          logger.trace(msg)
          const errorObject = {
            message: msg,
            code: 401
          }
          next(errorObject)
        } else {
          res.status(200).json({ result: rows })
        }
      }
    })
  }
}
