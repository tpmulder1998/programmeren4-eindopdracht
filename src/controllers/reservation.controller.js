const logger = require('../config/appconfig').logger
const database = require('../datalayer/mssql.dao')
const assert = require('assert')

module.exports = {
createReservation: function(req, res, next) {
    logger.info('Post /api/apartments/:apartmentId/reservations aangeroepen')

    // Check dat we de UserId in het request hebben - vanuit de validateToken functie.
    logger.trace('User id:', req.userId)

    // hier komt in het request een reservation binnen.
    const reservation = req.body
    const aId = req.params.apartmentId

    logger.info(reservation)
    try {
      // Valideer hier de properties van reservation.
    assert.equal(typeof reservation.ReservationId, 'string', 'reservation.reservationId is required.')


    var startDate = Date.parse(reservation.StartDate);
    var endDate = Date.parse(reservation.EndDate);

    if(startDate > endDate){
        alert('start date is greater than the end date.');
        logger.info('start date is greater than the end date. \n create aborted')
        return
    }

    } catch (e) {
      const errorObject = {
        message: e.toString(),
        code: 400
      }
      return next(errorObject)
    }

    const query =
      "INSERT INTO Reservation(ApartmentId, StartDate, EndDate, Status, UserId) VALUES ('" +
      aId +
      "','" +
      reservation.StartDate +
      "','" +
      reservation.EndDate +
      "','" +
      reservation.Status +
      "','" +
      req.userId +
      "'); SELECT * FROM Reservation JOIN DBUser ON Reservation.UserId = DBUser.UserId WHERE ReservationId = SCOPE_IDENTITY();"

    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
},

getAllReservations: function(req, res, next) {
    logger.info('Get /api/apartments/:apartmentId/reservations aangeroepen')

    const aId = req.params.apartmentId

    const query =
      `SELECT * FROM Reservation WHERE ApartmentId = ${aId}`
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
},

getReservationById: function(req, res, next) {
  logger.info('Get /api/apartments/:apartmentId/reservations/:reservationId aangeroepen')

  const aId = req.params.apartmentId
  const rId = req.params.reservationId

  const query =
    `SELECT * FROM Reservation WHERE ApartmentId = ${aId} AND ReservationId = ${rId}`
  database.executeQuery(query, (err, rows) => {
    // verwerk error of result
    if (err) {
      const errorObject = {
        message: 'Er ging iets mis in de database.',
        code: 500
      }
      next(errorObject)
    }
    if (rows) {
      res.status(200).json({ result: rows.recordset })
    }
  })
},

setReservationById: function(req, res, next) {

},

deleteReservationById: function(req, res, next) {
  logger.info('deleteById aangeroepen')
  const aId = req.params.apartmentId
  const rId = req.params.reservationId
  const userId = req.userId

  const query = `DELETE FROM Reservation WHERE ApartmentId=${aId} AND ReservationId=${rId} AND UserId=${userId}`
  database.executeQuery(query, (err, rows) => {
    // verwerk error of result
    if (err) {
      logger.trace('Could not delete apartment: ', err)
      const errorObject = {
        message: 'Er ging iets mis in de database.',
        code: 500
      }
      next(errorObject)
    }
    if (rows) {
      if (rows.rowsAffected[0] === 0) {
        // query ging goed, maar geen appartement met de gegeven ApartmentId EN userId.
        // -> retourneer een error!
        const msg = 'Apartment not found or you have no access to this apartment!'
        logger.trace(msg)
        const errorObject = {
          message: msg,
          code: 401
        }
        next(errorObject)
      } else {
        res.status(200).json({ result: rows })
      }
    }
  })
}
}