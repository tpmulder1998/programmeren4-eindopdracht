const logger = require('../config/appconfig').logger
const secretKey = require('../config/appconfig').secretKey
const jwt = require('jsonwebtoken')
const database = require('../datalayer/mssql.dao')
const assert = require('assert')

const phoneValidator = new RegExp('^06(| |-)[0-9]{8}$')

module.exports = {
  registerUser: (req, res, next) => {
    logger.info('registerUser aangeroepen')

    // user informatie uit req.body halen
    const user = req.body
    // Verifieer dat de juiste velden aanwezig zijn.
    
    

    try {
      assert.equal(typeof user.FirstName, 'string', 'firstName is required.')
      assert(phoneValidator.test(user.PhoneNumber), 'A valid phoneNumber is required.')
    } catch (ex) {
      const errorObject = {
        message: 'Validation fails: ' + ex.toString(),
        code: 500
      }
      return next(errorObject)
    }

    // INSERT query samenstellen
    const query =
      `INSERT INTO [DBUser] (FirstName, LastName, StreetAddress, PostalCode, City, DateOfBirth, PhoneNumber, EmailAddress, Password)` +
      `VALUES ('${user.FirstName}', '${user.LastName}', '${user.StreetAddress}', ` +
      `'${user.PostCode}', '${user.City}', '${user.DateOfBirth}',` +
      `${user.PhoneNumber}, '${user.EmailAddress}', '${user.Password}')` +
      `; SELECT SCOPE_IDENTITY() AS UserId`

    // Query uitvoeren en resultaat retourneren.

    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  },

  loginUser: (req, res, next) => {
    logger.info('loginUser aangeroepen')

    // user informatie uit req.body halen
    const user = req.body
    
    logger.info(req.body)

    // SELECT query samenstellen
    const query = `SELECT Password, UserId FROM [DBUser] WHERE EmailAddress='${user.EmailAddress}'`
    // Query uitvoeren en resultaat retourneren.
    database.executeQuery(query, (err, rows) => {
      // verwerk error of result
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        // Als we hier zijn:
        if (rows.recordset.length === 0 || user.Password !== rows.recordset[0].Password) {
          logger.info(user.Password)
          const errorObject = {
            message: 'Geen toegang: email bestaat niet of password is niet correct!',
            code: 401
          }
          next(errorObject)
        } else {
          logger.info('Password match, user logged id')
          logger.trace(rows.recordset)

          // Maak de payload, die we in het token stoppen.
          // De payload kunnen we er bij het verifiëren van het token later weer uithalen.
          const payload = {
            UserId: rows.recordset[0].UserId
          }
          jwt.sign({ data: payload }, secretKey, { expiresIn: 60 * 60 }, (err, token) => {
            if (err) {
              const errorObject = {
                message: 'Kon geen JWT genereren.',
                code: 500
              }
              next(errorObject)
            }
            if (token) {
              res.status(200).json({
                result: {
                  token: token
                }
              })
            }
          })
        }
      }
    })
  },

  validateToken: (req, res, next) => {
    logger.info('validateToken aangeroepen')
    // logger.debug(req.headers)
    const authHeader = req.headers.authorization
    if (!authHeader) {
      errorObject = {
        message: 'Authorization header missing!',
        code: 401
      }
      logger.warn('Validate token failed: ', errorObject.message)
      return next(errorObject)
    }
    const token = authHeader.substring(7, authHeader.length)

    jwt.verify(token, secretKey, (err, payload) => {
      if (err) {
        errorObject = {
          message: 'not authorized',
          code: 401
        }
        logger.warn('Validate token failed: ', errorObject.message)
        next(errorObject)
      }
      logger.trace('payload', payload)
      if (payload.data && payload.data.UserId) {
        logger.debug('token is valid', payload)
        // User heeft toegang. Voeg UserId uit payload toe aan
        // request, voor ieder volgend endpoint.
        req.userId = payload.data.UserId
        next()
      } else {
        errorObject = {
          message: 'UserId is missing!',
          code: 401
        }
        logger.warn('Validate token failed: ', errorObject.message)
        next(errorObject)
      }
    })
  },

  getAll: (req, res, next) => {
    logger.info('getAll aangeroepen')

    // query samenstellen met user data
    const query = `SELECT * FROM [DBUser]`

    // Query aanroepen op de database
    database.executeQuery(query, (err, rows) => {
      if (err) {
        const errorObject = {
          message: 'Er ging iets mis in de database.',
          code: 500
        }
        next(errorObject)
      }
      if (rows) {
        res.status(200).json({ result: rows.recordset })
      }
    })
  }
}
